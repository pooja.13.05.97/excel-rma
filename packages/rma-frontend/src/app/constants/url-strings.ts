export const LIST_SALES_INVOICE_ENDPOINT = 'api/sales_invoice/v1/list';
export const DIRECT_PROFILE_ENDPOINT = '/api/direct/v1/profile';
export const SALES_INVOICE_GET_ONE_ENDPOINT = '/api/sales_invoice/v1/get/';
