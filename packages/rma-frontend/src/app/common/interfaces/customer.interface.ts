export interface Customer {
  uuid: string;
  name: string;
  addressLine1: string;
  addressLine2: string;
  city: string;
  pinCode: string;
}
