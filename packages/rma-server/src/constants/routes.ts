export const FRAPPE_API_GET_USER_INFO_ENDPOINT = '/api/resource/User/';
export const CLIENT_CALLBACK_ENDPOINT = '/home/callback';
export const FRAPPE_API_GET_OAUTH_BEARER_TOKEN_ENDPOINT =
  '/api/resource/OAuth%20Bearer%20Token/';
export const FRAPPE_API_GET_CUSTOMER_ENDPOINT = '/api/resource/Customer/';
export const FRAPPE_API_GET_ITEM_ENDPOINT = '/api/resource/Item/';
export const TOKEN_ADD_ENDPOINT = '/api/connect/v1/token_added';
export const TOKEN_DELETE_ENDPOINT = '/api/connect/v1/token_delete';
export const SUPPLIER_AFTER_INSERT_ENDPOINT = '/api/supplier/webhook/v1/create';
export const SUPPLIER_ON_UPDATE_ENDPOINT = '/api/supplier/webhook/v1/update';
export const SUPPLIER_ON_TRASH_ENDPOINT = '/api/supplier/webhook/v1/delete';
export const CUSTOMER_AFTER_INSERT_ENDPOINT = '/api/customer/webhook/v1/create';
export const CUSTOMER_ON_UPDATE_ENDPOINT = '/api/customer/webhook/v1/update';
export const CUSTOMER_ON_TRASH_ENDPOINT = '/api/customer/webhook/v1/delete';
export const ITEM_AFTER_INSERT_ENDPOINT = '/api/item/webhook/v1/create';
export const ITEM_ON_UPDATE_ENDPOINT = '/api/item/webhook/v1/update';
export const ITEM_ON_TRASH_ENDPOINT = '/api/item/webhook/v1/delete';
export const FRAPPE_API_SERIAL_NO_ENDPOINT = '/api/resource/Serial%20No';
export const FRAPPE_API_SALES_INVOICE_ENDPOINT =
  '/api/resource/Sales%20Invoice';
export const SERIAL_NO_AFTER_INSERT_ENDPOINT =
  '/api/serial_no/webhook/v1/create';
export const SERIAL_NO_ON_UPDATE_ENDPOINT = '/api/serial_no/webhook/v1/update';
export const OAUTH_BEARER_TOKEN_ENDPOINT =
  '/api/resource/OAuth%20Bearer%20Token';
export const GET_TIME_ZONE_ENDPOINT = '/api/method/frappe.client.get_time_zone';
export const LIST_DELIVERY_NOTE_ENDPOINT = '/api/resource/Delivery%20Note';
