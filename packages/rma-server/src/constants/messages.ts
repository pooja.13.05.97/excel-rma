export const SETUP_ALREADY_COMPLETE = 'Setup already complete';
export const PLEASE_RUN_SETUP = 'Please run setup';
export const SOMETHING_WENT_WRONG = 'Something went wrong';
export const NOT_CONNECTED = 'not connected';
export const SERVICE_ALREADY_REGISTERED = 'Service already registered';
export const INVALID_CODE = 'Invalid Code';
export const INVALID_STATE = 'Invalid State';
export const INVALID_FRAPPE_TOKEN = 'Invalid Frappe Token';
export const SUBMITTED_SALES_INVOICE_CANNOT_BE_UPDATED =
  'submitted Sales Invoice cannot be updated';
export const DELIVERY_NOTE_ALREADY_SUBMITTED =
  'Provided delivery note is already submitted';
export const DELIVERY_NOTE_IN_QUEUE = 'Provided delivery note is in queue';
export const SERIAL_NO_ALREADY_EXIST = 'Serial Number already exist';
export const ITEM_NOT_FOUND = 'Item not found';
export const CUSTOMER_NOT_FOUND = 'Customer not found';
export const SALES_INVOICE_NOT_FOUND = 'Sales Invoice not found';
